const path = require('path')
const fs = require('fs')
const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)

app.use(express.json());
app.use(express.static(path.join(__dirname, 'public', 'static')))

// stocks.json contains the stocks being tracked and their current price. 
// When a new server instance is created, the content of stocks.json is saved to a global variable
// which is used in all route handlers.
let stockArray = []
fs.readFile('./stocks.json', (err, stockData) => {
    stockArray = JSON.parse(stockData)
})

// Serving the html page
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

// Serving the stock data at /current
app.get('/current', (req, res) => {
    res.json(stockArray)
})

// At a PATCH request the route handler checks if the stock name from the request matches any existing in stockArray
// If a match is found, the stock price in stockArray is updated and emitted to all connected clients in the stockUpdate event
// A positive response with the request in JSON is sent back and stocks.json is rewritten with the new stock data
// If no match is found, error is printed to the console and a negative response sent back 
app.patch('/update', (req, res) => {
    let foundStockMatch = false
    for (stock of stockArray) {
            
        if (req.body.data.stockName === stock.data.stockName) {
                stock.data.price = req.body.data.price
                foundStockMatch = true
        }
    }
    if (foundStockMatch) {
        io.emit('stockUpdate', JSON.stringify(stockArray))
        res.status(200).json(JSON.stringify(req.body))

        fs.writeFile('./stocks.json', JSON.stringify(stockArray), (err) => {})
    } else {
        console.log('Error! Attempt was made to update stock that does not exist.')
        res.status(404).json('{"Error": "Error! Attempt was made to update stock that does not exist."}')
    }
    
})

io.on('connection', (socket) => {
    console.log(`Client ${socket.id} connected`)
    

    socket.on('disconnect', () => {
        console.log(`Client ${socket.id} disconnected`)
    })
})

server.listen(3000)

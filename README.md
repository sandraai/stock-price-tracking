# Stock price tracking

This application serves a webpage which keeps track of stock prices for a set of stocks. 

## Installation & dependencies

Install NodeJS

Install dependencies by running `npm install`

## Run application

Run the application with `node main.js`, then open [localhost:3000](http://localhost:3000) in your web browser.